/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.midterm2_algorithm;

import java.util.Scanner;

/**
 *
 * @author Ow
 */
public class Midterm_02 {
    
    static long totalTime;
    static long startTime;
    static long stopTime;
    
    public static int findChildren(int n, int k) {
        
        if (n == 1) {
            return 1;
        }else {
            return (findChildren(n-1, k) + k - 1) % n + 1;
        }
    }
    
    public static void main(String[] args) {
        
        Scanner wn = new Scanner(System.in);
        int n;
        int k;
        
        n = wn.nextInt();
        k = wn.nextInt();
        
        startTime = System.nanoTime();
        System.out.println("If any "+n+" children and k = "+k+ " then child number " +findChildren(n, k)+" win !!");
        stopTime = System.nanoTime();
        
        totalTime = stopTime - startTime;
        System.out.println("Time: " + totalTime + " nanoseconds");
        
    }
}
